package com.ecore.roles.web.rest;

import com.ecore.roles.model.Role;
import com.ecore.roles.service.MembershipsService;
import com.ecore.roles.service.RolesService;
import com.ecore.roles.web.RolesApi;
import com.ecore.roles.web.dto.RoleDto;
import lombok.RequiredArgsConstructor;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.List;
import java.util.UUID;
import java.util.stream.Collectors;

@RequiredArgsConstructor
@RestController
@RequestMapping("/v1/roles")
public class RolesRestController implements RolesApi {

    private final RolesService rolesService;

    private final MembershipsService membershipsService;

    @PostMapping(consumes = "application/json", produces = "application/json")
    public ResponseEntity<RoleDto> createRole(@Valid @RequestBody RoleDto roleDto) {
        Role createdRole = rolesService.createRole(roleDto.toModel());
        RoleDto createdRoleDto = RoleDto.fromModel(createdRole);
        return ResponseEntity.status(HttpStatus.CREATED).body(createdRoleDto);
    }

    @GetMapping(produces = "application/json")
    public ResponseEntity<List<RoleDto>> getRoles() {
        List<Role> roles = rolesService.getRoles();
        List<RoleDto> roleDtos = roles.stream().map(RoleDto::fromModel).collect(Collectors.toList());
        return ResponseEntity.ok(roleDtos);
    }

    @GetMapping(path = "/{roleId}", produces = "application/json")
    public ResponseEntity<RoleDto> getRole(@PathVariable UUID roleId) {
        Role role = rolesService.getRole(roleId);
        RoleDto roleDto = RoleDto.fromModel(role);
        return ResponseEntity.ok(roleDto);
    }

    @GetMapping(path = "/search", produces = {"application/json"})
    public ResponseEntity<RoleDto> getRole(
            @RequestParam("teamMemberId") UUID userId,
            @RequestParam("teamId") UUID teamId) {
        return membershipsService.findByUserIdAndTeamId(userId, teamId)
                .map(membership -> ResponseEntity.status(HttpStatus.OK).body(RoleDto.fromModel(membership.getRole())))
                .orElse(ResponseEntity.noContent().build());
    }
}
