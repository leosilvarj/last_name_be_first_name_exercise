package com.ecore.roles.web.rest;

import com.ecore.roles.model.Membership;
import com.ecore.roles.service.MembershipsService;
import com.ecore.roles.web.MembershipsApi;
import com.ecore.roles.web.dto.MembershipDto;
import lombok.RequiredArgsConstructor;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.List;
import java.util.UUID;
import java.util.stream.Collectors;

@RequiredArgsConstructor
@RestController
@RequestMapping(value = "/v1/roles/memberships")
public class MembershipsRestController implements MembershipsApi {

    private final MembershipsService membershipsService;

    @PostMapping(consumes = "application/json", produces = "application/json")
    public ResponseEntity<MembershipDto> assignRoleToMembership(
            @Valid @RequestBody MembershipDto membershipDto) {
        Membership assignedMembership = membershipsService.assignRoleToMembership(membershipDto.toModel());
        MembershipDto assignedMembershipDto = MembershipDto.fromModel(assignedMembership);
        return ResponseEntity.status(HttpStatus.CREATED).body(assignedMembershipDto);
    }

    @GetMapping(path = "/search", produces = "application/json")
    public ResponseEntity<List<MembershipDto>> getMemberships(@RequestParam UUID roleId) {
        List<Membership> memberships = membershipsService.getMemberships(roleId);
        List<MembershipDto> membershipDtos = memberships.stream()
                .map(MembershipDto::fromModel)
                .collect(Collectors.toList());
        return ResponseEntity.ok(membershipDtos);
    }
}
