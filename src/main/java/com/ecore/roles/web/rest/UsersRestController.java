package com.ecore.roles.web.rest;

import com.ecore.roles.service.UsersService;
import com.ecore.roles.web.UsersApi;
import com.ecore.roles.web.dto.UserDto;
import lombok.RequiredArgsConstructor;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;
import java.util.UUID;
import java.util.stream.Collectors;

@RequiredArgsConstructor
@RestController
@RequestMapping("/v1/users")
public class UsersRestController implements UsersApi {

    private final UsersService usersService;

    @GetMapping(produces = "application/json")
    public ResponseEntity<List<UserDto>> getUsers() {
        List<UserDto> userDtos = usersService.getUsers().stream()
                .map(UserDto::fromModel)
                .collect(Collectors.toList());
        return ResponseEntity.ok(userDtos);
    }

    @GetMapping(path = "/{userId}", produces = "application/json")
    public ResponseEntity<UserDto> getUser(@PathVariable UUID userId) {
        UserDto userDto = UserDto.fromModel(usersService.getUser(userId));
        return ResponseEntity.ok(userDto);
    }
}
