package com.ecore.roles.web.rest;

import com.ecore.roles.service.TeamsService;
import com.ecore.roles.web.TeamsApi;
import com.ecore.roles.web.dto.TeamDto;
import lombok.RequiredArgsConstructor;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;
import java.util.UUID;
import java.util.stream.Collectors;

@RequiredArgsConstructor
@RestController
@RequestMapping("/v1/teams")
public class TeamsRestController implements TeamsApi {

    private final TeamsService teamsService;

    @GetMapping(produces = "application/json")
    public ResponseEntity<List<TeamDto>> getTeams() {
        List<TeamDto> teamDtos = teamsService.getTeams().stream()
                .map(TeamDto::fromModel)
                .collect(Collectors.toList());
        return ResponseEntity.ok(teamDtos);
    }

    @GetMapping(path = "/{teamId}", produces = "application/json")
    public ResponseEntity<TeamDto> getTeam(@PathVariable UUID teamId) {
        TeamDto teamDto = TeamDto.fromModel(teamsService.getTeam(teamId));
        return ResponseEntity.ok(teamDto);
    }
}
