package com.ecore.roles.service;

import com.ecore.roles.client.TeamsClient;
import com.ecore.roles.client.model.Team;
import com.ecore.roles.configuration.ClientsConfigurationProperties;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.mockito.junit.jupiter.MockitoExtension;
import org.springframework.core.ParameterizedTypeReference;
import org.springframework.http.HttpMethod;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.client.RestTemplate;

import java.util.Arrays;
import java.util.List;
import java.util.UUID;

import static com.ecore.roles.utils.TestData.*;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotNull;
import static org.mockito.Mockito.*;

@ExtendWith(MockitoExtension.class)
class TeamsServiceTest {

    @Mock
    private TeamsClient teamsClient;
    @Mock
    private RestTemplate restTemplate;
    @Mock
    private ClientsConfigurationProperties clientsConfigurationProperties;

    @BeforeEach
    void setUp() {
        MockitoAnnotations.openMocks(this);
        teamsClient = new TeamsClient(restTemplate, clientsConfigurationProperties);
    }

    @Test
    void testGetTeamReturnsTeamResponseEntity() {
        UUID teamId = ORDINARY_CORAL_LYNX_TEAM_UUID;
        String teamsApiHost = "urlApi";
        String apiUrl = teamsApiHost + "/" + teamId;

        Team expectedTeam = ORDINARY_CORAL_LYNX_TEAM();

        when(clientsConfigurationProperties.getTeamsApiHost()).thenReturn(teamsApiHost);
        when(restTemplate.exchange(eq(apiUrl), eq(HttpMethod.GET), eq(null), eq(Team.class)))
                .thenReturn(ResponseEntity.ok(expectedTeam));

        ResponseEntity<Team> result = teamsClient.getTeam(teamId);

        assertNotNull(result);
        assertEquals(ResponseEntity.ok(expectedTeam), result);
        verify(clientsConfigurationProperties, times(1)).getTeamsApiHost();
        verify(restTemplate, times(1)).exchange(eq(apiUrl), eq(HttpMethod.GET), eq(null), eq(Team.class));
    }

    @Test
    void testGetTeamsReturnsTeams() {
        String apiUrl = "urlApi";
        List<Team> expectedTeams =
                Arrays.asList(WEEKLY_PEACH_WILDEBEEST_TEAM(true), ORDINARY_CORAL_LYNX_TEAM(true));

        when(clientsConfigurationProperties.getTeamsApiHost()).thenReturn(apiUrl);
        when(restTemplate.exchange(eq(apiUrl), eq(HttpMethod.GET), eq(null),
                eq(new ParameterizedTypeReference<List<Team>>() {})))
                        .thenReturn(new ResponseEntity<>(expectedTeams, HttpStatus.OK));

        ResponseEntity<List<Team>> result = teamsClient.getTeams();

        assertNotNull(result);
        assertEquals(expectedTeams, result.getBody());
        verify(clientsConfigurationProperties, times(1)).getTeamsApiHost();
        verify(restTemplate, times(1)).exchange(eq(apiUrl), eq(HttpMethod.GET), eq(null),
                eq(new ParameterizedTypeReference<List<Team>>() {}));
    }
}
