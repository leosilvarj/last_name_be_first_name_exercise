package com.ecore.roles.service;

import com.ecore.roles.client.model.Team;
import com.ecore.roles.exception.InvalidArgumentException;
import com.ecore.roles.exception.ResourceExistsException;
import com.ecore.roles.model.Membership;
import com.ecore.roles.model.Role;
import com.ecore.roles.repository.MembershipRepository;
import com.ecore.roles.repository.RoleRepository;
import com.ecore.roles.service.impl.MembershipsServiceImpl;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;
import java.util.UUID;

import static com.ecore.roles.utils.TestData.*;
import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.*;

@ExtendWith(MockitoExtension.class)
class MembershipsServiceTest {

    @InjectMocks
    private MembershipsServiceImpl membershipsService;
    @Mock
    private MembershipRepository membershipRepository;
    @Mock
    private RoleRepository roleRepository;
    @Mock
    private UsersService usersService;
    @Mock
    private TeamsService teamsService;

    @Test
    public void testAssignRoleToMembership() {
        Membership defaultMembership = DEFAULT_MEMBERSHIP();
        Role role = DEVELOPER_ROLE();

        Team team = new Team();
        team.setId(defaultMembership.getTeamId());
        team.setTeamLeadId(GIANNI_USER_UUID);
        List<UUID> teamMemberIds = new ArrayList<>();
        teamMemberIds.add(GIANNI_USER_UUID);
        team.setTeamMemberIds(teamMemberIds);

        when(teamsService.getTeam(defaultMembership.getTeamId())).thenReturn(team);
        when(roleRepository.findById(role.getId())).thenReturn(Optional.of(role));
        when(membershipRepository.findByUserIdAndTeamId(defaultMembership.getUserId(), defaultMembership.getTeamId()))
                .thenReturn(Optional.empty());
        when(membershipRepository.save(defaultMembership)).thenReturn(defaultMembership);

        Membership result = membershipsService.assignRoleToMembership(defaultMembership);

        verify(teamsService, times(1)).getTeam(defaultMembership.getTeamId());
        verify(roleRepository, times(1)).findById(role.getId());
        verify(membershipRepository, times(1)).findByUserIdAndTeamId(defaultMembership.getUserId(), defaultMembership.getTeamId());
        verify(membershipRepository, times(1)).save(defaultMembership);

        assertNotNull(result);
        assertEquals(defaultMembership, result);
    }

    @Test
    public void shouldFailToCreateMembershipWhenMembershipsIsNull() {
        assertThrows(NullPointerException.class,
                () -> membershipsService.assignRoleToMembership(null));
    }

    @Test
    public void shouldFailToCreateMembershipWhenItExists() {
        Membership expectedMembership = DEFAULT_MEMBERSHIP();

        when(roleRepository.findById(expectedMembership.getRole().getId())).thenReturn(Optional.of(expectedMembership.getRole()));
        when(teamsService.getTeam(expectedMembership.getTeamId())).thenReturn(ORDINARY_CORAL_LYNX_TEAM());
        when(membershipRepository.findByUserIdAndTeamId(expectedMembership.getUserId(), expectedMembership.getTeamId()))
                .thenReturn(Optional.of(expectedMembership));

        ResourceExistsException exception = assertThrows(ResourceExistsException.class,
                () -> membershipsService.assignRoleToMembership(expectedMembership));

        assertEquals("Membership already exists", exception.getMessage());
        verify(roleRepository, times(1)).findById(any());
        verify(teamsService, times(1)).getTeam(any());
    }

    @Test
    public void shouldFailToCreateMembershipWhenItHasInvalidRole() {
        Membership expectedMembership = DEFAULT_MEMBERSHIP();
        expectedMembership.setRole(null);

        InvalidArgumentException exception = assertThrows(InvalidArgumentException.class,
                () -> membershipsService.assignRoleToMembership(expectedMembership));

        assertEquals("Invalid 'Role' object", exception.getMessage());
        verify(membershipRepository, times(0)).findByUserIdAndTeamId(any(), any());
        verify(roleRepository, times(0)).getById(any());
        verify(usersService, times(0)).getUser(any());
        verify(teamsService, times(0)).getTeam(any());
    }

    @Test
    public void shouldFailToGetMembershipsWhenRoleIdIsNull() {
        assertThrows(NullPointerException.class,
                () -> membershipsService.getMemberships(null));
    }

    @Test
    void testGetMembershipsReturnsListOfMemberships() {
        UUID roleId = UUID.randomUUID();

        List<Membership> expectedMemberships = new ArrayList<>();
        expectedMemberships.add(Membership.builder().id(UUID.randomUUID()).build());
        expectedMemberships.add(Membership.builder().id(UUID.randomUUID()).build());

        when(membershipRepository.findByRoleId(roleId)).thenReturn(expectedMemberships);

        List<Membership> result = membershipsService.getMemberships(roleId);

        assertNotNull(result);
        assertEquals(expectedMemberships.size(), result.size());
        assertEquals(expectedMemberships.get(0), result.get(0));
        assertEquals(expectedMemberships.get(1), result.get(1));
        verify(membershipRepository, times(1)).findByRoleId(roleId);
    }

    @Test
    void testGetMembershipsReturnsEmptyListIfNoMembershipsFound() {
        UUID roleId = UUID.randomUUID();

        when(membershipRepository.findByRoleId(roleId)).thenReturn(new ArrayList<>());

        List<Membership> result = membershipsService.getMemberships(roleId);

        assertNotNull(result);
        assertTrue(result.isEmpty());
        verify(membershipRepository, times(1)).findByRoleId(roleId);
    }
}
