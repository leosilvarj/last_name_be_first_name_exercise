package com.ecore.roles.service;

import com.ecore.roles.exception.ResourceExistsException;
import com.ecore.roles.exception.ResourceNotFoundException;
import com.ecore.roles.model.Role;
import com.ecore.roles.repository.RoleRepository;
import com.ecore.roles.service.impl.RolesServiceImpl;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import static com.ecore.roles.utils.TestData.*;
import static java.lang.String.format;
import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.Mockito.*;

@ExtendWith(MockitoExtension.class)
class RolesServiceTest {

    @InjectMocks
    private RolesServiceImpl rolesService;

    @Mock
    private RoleRepository roleRepository;

    @Test
    public void shouldCreateRole() {
        Role developerRole = DEVELOPER_ROLE();
        when(roleRepository.save(developerRole)).thenReturn(developerRole);

        Role role = rolesService.createRole(developerRole);

        assertNotNull(role);
        assertEquals(developerRole, role);
    }

    @Test
    void testCreateRoleWithValidRoleReturnsRole() {
        Role role = DEVELOPER_ROLE();
        when(roleRepository.findByName(role.getName()))
                .thenReturn(Optional.empty());
        when(roleRepository.save(role))
                .thenReturn(role);

        Role result = rolesService.createRole(role);

        assertNotNull(result);
        assertEquals(role, result);
        verify(roleRepository, times(1)).findByName(role.getName());
        verify(roleRepository, times(1)).save(role);
    }

    @Test
    void testCreateRoleWithExistingRoleThrowsResourceExistsException() {
        Role role = DEVELOPER_ROLE();
        when(roleRepository.findByName(role.getName()))
                .thenReturn(Optional.of(role));

        assertThrows(ResourceExistsException.class, () -> rolesService.createRole(role));

        verify(roleRepository, times(1)).findByName(role.getName());
        verify(roleRepository, never()).save(any(Role.class));
    }

    @Test
    public void shouldFailToCreateRoleWhenRoleIsNull() {
        assertThrows(NullPointerException.class,
                () -> rolesService.createRole(null));
    }

    @Test
    public void shouldReturnRoleWhenRoleIdExists() {
        Role developerRole = DEVELOPER_ROLE();
        when(roleRepository.findById(developerRole.getId())).thenReturn(Optional.of(developerRole));

        Role role = rolesService.getRole(developerRole.getId());

        assertNotNull(role);
        assertEquals(developerRole, role);
    }

    @Test
    public void shouldFailToGetRoleWhenRoleIdDoesNotExist() {
        ResourceNotFoundException exception = assertThrows(ResourceNotFoundException.class,
                () -> rolesService.getRole(UUID_1));

        assertEquals(format("Role %s not found", UUID_1), exception.getMessage());
    }

    @Test
    void testGetRolesReturnsListOfRoles() {
        List<Role> expectedRoles = new ArrayList<>();
        expectedRoles.add(DEVELOPER_ROLE());
        expectedRoles.add(PRODUCT_OWNER_ROLE());

        when(roleRepository.findAll()).thenReturn(expectedRoles);

        List<Role> result = rolesService.getRoles();

        assertNotNull(result);
        assertEquals(expectedRoles.size(), result.size());
        assertEquals(expectedRoles.get(0), result.get(0));
        assertEquals(expectedRoles.get(1), result.get(1));
        verify(roleRepository, times(1)).findAll();
    }

    @Test
    void testGetRolesReturnsEmptyListIfNoRolesFound() {
        when(roleRepository.findAll()).thenReturn(new ArrayList<>());

        List<Role> result = rolesService.getRoles();

        assertNotNull(result);
        assertTrue(result.isEmpty());
        verify(roleRepository, times(1)).findAll();
    }

}
