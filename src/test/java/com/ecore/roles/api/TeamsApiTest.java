package com.ecore.roles.api;

import com.ecore.roles.client.model.Team;
import com.ecore.roles.client.model.User;
import com.ecore.roles.utils.RestAssuredHelper;
import com.ecore.roles.web.dto.TeamDto;
import com.ecore.roles.web.dto.UserDto;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.web.server.LocalServerPort;
import org.springframework.test.web.client.MockRestServiceServer;
import org.springframework.web.client.RestTemplate;

import static com.ecore.roles.utils.MockUtils.mockGetTeamById;
import static com.ecore.roles.utils.MockUtils.mockGetTeams;
import static com.ecore.roles.utils.RestAssuredHelper.getTeam;
import static com.ecore.roles.utils.RestAssuredHelper.getTeams;
import static com.ecore.roles.utils.TestData.*;
import static org.assertj.core.api.Assertions.assertThat;

@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT)
public class TeamsApiTest {

    private final RestTemplate restTemplate;

    private MockRestServiceServer mockServer;

    @LocalServerPort
    private int port;

    @Autowired
    public TeamsApiTest(RestTemplate restTemplate) {
        this.restTemplate = restTemplate;
    }

    @BeforeEach
    void setUp() {
        mockServer = MockRestServiceServer.createServer(restTemplate);
        RestAssuredHelper.setUp(port);
    }

    @Test
    void shouldGetTeam() {
        Team expectedTeam = ORDINARY_CORAL_LYNX_TEAM();
        mockGetTeamById(mockServer, ORDINARY_CORAL_LYNX_TEAM_UUID, ORDINARY_CORAL_LYNX_TEAM());
        TeamDto actualTeams = getTeam(ORDINARY_CORAL_LYNX_TEAM_UUID)
                .statusCode(200)
                .extract().as(TeamDto.class);

        assertThat(actualTeams.getId()).isNotNull();
        assertThat(actualTeams).isEqualTo(TeamDto.fromModel(expectedTeam));
    }

    @Test
    void shouldGetAllTeam() {
        mockGetTeams(mockServer, ORDINARY_CORAL_LYNX_TEAM());
        TeamDto[] teams = getTeams()
                .statusCode(200)
                .extract().as(TeamDto[].class);

        assertThat(teams.length).isEqualTo(1);
        assertThat(teams[0].getId()).isNotNull();
    }

    @Test
    void testFromModel() {
        User user = GIANNI_USER();
        UserDto userDto = UserDto.fromModel(GIANNI_USER());
        Assertions.assertNotNull(userDto);
        Assertions.assertEquals(user.getId(), userDto.getId());
        Assertions.assertEquals(user.getDisplayName(), userDto.getDisplayName());
        Assertions.assertEquals(user.getFirstName(), userDto.getFirstName());
        Assertions.assertEquals(user.getLastName(), userDto.getLastName());
    }
}
